car = {
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964
    }

x = car.keys()
print(x) #before the change
car["color"] = "white"
print(x) #after the change

x = car.values()
print(x)

y = car.items()
print(y)

thisdict = {
      "brand": "Ford",
      "model": "Mustang",
      "year": 1964
    }
if "model" in thisdict:
    print("Yes, 'model' is one of the keys in the thisdict dictionary")

thisdict =	{
          "brand": "Ford",
          "model": "Mustang",
          "year": 1964
        }

thisdict["year"] = 2018
print(thisdict)

thisdict = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
thisdict.update({"color": "red"})
print(thisdict)

thisdict =	{
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
for x in thisdict:
    print(thisdict[x])

for x in thisdict.keys():
    print(x)

thisdict = {
      "brand": "Ford",
      "model": "Mustang",
      "year": 1964
    }
mydict = thisdict.copy()
print(mydict)