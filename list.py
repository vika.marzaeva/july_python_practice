my_list = ['banana', 'Orange', 'Kiwi', 'cherry', 'banana']
my_list.sort(reverse=True)
my_list.append('apple')
print(my_list.count('banana'))
my_list.insert(2, 'pear')
my_list.remove('Kiwi')
print(my_list)

thislist = ["apple", "banana", "cherry"]
mylist = thislist.copy()
print('mylist = ', mylist)

fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
newlist = []

for x in fruits:
  if "a" in x:
    newlist.append(x)

print('newlist = ', newlist)
