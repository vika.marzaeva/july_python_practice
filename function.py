#source https://realpython.com/lessons/basics-python-inner-functions/

def outer_fun():
    def inner_fn():
        print("Hello inside!")
    print("Hello outside!")
    inner_fn()

def mug(stuff):
    def inside():
        print(f"Yammy {stuff}")
    inside()

def arithmetic(a, b , operation):
    if operation == "+":
        print(a+b)
    elif operation == "-":
        print(a-b)
    elif operation == "*":
        print(a*b)
    elif operation == "/":
        print(a/b)
    else:
        print("Unknown operation")

def is_year_leap(year):
    if (year % 4 == 0) and (year % 100 != 0) or (year % 400 == 0):
        print('This is leap year')
    else:
        print('This does not leap year')

def square(a):
    P = 4 * a
    S = a * a
    d = round((2**(0.5) * a),2)
    print(f"P = {P}, S = {S}, d = {d}")

outer_fun()
mug("Coffee")
arithmetic(3, 5, "+")
is_year_leap(2021)
square(2)


